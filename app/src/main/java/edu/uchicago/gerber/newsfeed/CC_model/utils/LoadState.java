package edu.uchicago.gerber.newsfeed.CC_model.utils;

public enum LoadState {
    DONE, LOADING, ERROR, NODATA
}
