package edu.uchicago.gerber.newsfeed.AA_view.views;

import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.paging.PagedList;
import androidx.recyclerview.widget.RecyclerView;

import org.greenrobot.eventbus.EventBus;

import edu.uchicago.gerber.newsfeed.AA_view.adapters.BooksListAdapter;
import edu.uchicago.gerber.newsfeed.AA_view.adapters.RetryCallback;
import edu.uchicago.gerber.newsfeed.BB_viewmodel.viewmodels.BooksListViewModel;
import edu.uchicago.gerber.newsfeed.CC_model.cache.Cache;
import edu.uchicago.gerber.newsfeed.CC_model.models.Item;
import edu.uchicago.gerber.newsfeed.CC_model.utils.LoadState;
import edu.uchicago.gerber.newsfeed.R;


public class ResultsListFragment extends Fragment {
    private TextView txtError, txtNoData;
    private ProgressBar progressBar;
    private RecyclerView recyclerView;

    private BooksListViewModel viewModel = null;
    private BooksListAdapter booksListAdapter = null;

    private TextView headerTitle;
    private Button imgShare, imgNavigation;

    private String keyword;
    public static EditText keywordEditText;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View containerView = inflater.inflate(R.layout.frag_book_list, container, false);

        viewModel = new ViewModelProvider(this).get(BooksListViewModel.class);
        txtError = containerView.findViewById(R.id.txt_error);
        txtNoData = containerView.findViewById(R.id.txt_no_data);
        progressBar = containerView.findViewById(R.id.progress_bar);
        recyclerView = containerView.findViewById(R.id.recycler_view);
        keywordEditText = containerView.findViewById(R.id.edittext_booksearch_keyword);
        keywordEditText.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int i, KeyEvent keyEvent) {
                if (keyEvent.getKeyCode() == KeyEvent.KEYCODE_ENTER) {

                    //consume the ACTION_UP, and only activate upon ACTION_DOWN
                    if (keyEvent.getAction()==KeyEvent.ACTION_UP) return true;

                    recyclerView.stopScroll();
                    recyclerView.getRecycledViewPool().clear();
                    keyword = keywordEditText.getEditableText().toString().trim();
                    Cache.getInstance().setKeyword(keyword);
                    //Emit to the EventBus to invalidate any previous observers to make way for new observers in the BooksPagedKeyedDataSource
                    EventBus.getDefault().post(keyword);
                    booksListAdapter.notifyDataSetChanged();
                    return true;
                }
                return false;

            }
        });
        keywordEditText.setText(Cache.getInstance().getKeyword());
        initAdapter();
        initState();
        setToolBarToFragment(containerView);

        return containerView;
    }


    private void setToolBarToFragment(View view) {
        headerTitle = view.findViewById(R.id.header_title);
        headerTitle.setText(getResources().getString(R.string.article_list));
        imgShare = view.findViewById(R.id.imgShare);
        imgNavigation = view.findViewById(R.id.imgNavigation);

    }


    private void initState() {

        txtError.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewModel.retry();
            }
        });

        viewModel.getState().observe(getActivity(), new Observer<LoadState>() {
            @Override
            public void onChanged(LoadState state) {
                MainActivity.hideKeyboardFrom(getContext(), ResultsListFragment.this.getView());
                recyclerView.setVisibility(View.VISIBLE);

                if (viewModel.listIsEmpty() && state == LoadState.LOADING) {
                    progressBar.setVisibility(View.VISIBLE);
                } else {
                    progressBar.setVisibility(View.GONE);
                }
                if (viewModel.listIsEmpty() && state == LoadState.ERROR) {
                    txtError.setVisibility(View.VISIBLE);
                } else {
                    txtError.setVisibility(View.GONE);
                }


                if (state == LoadState.NODATA) {
                    recyclerView.setVisibility(View.GONE);
                    txtNoData.setVisibility(View.VISIBLE);
                } else {
                    recyclerView.setVisibility(View.VISIBLE);
                    txtNoData.setVisibility(View.GONE);
                }

                if (!viewModel.listIsEmpty()) {
                    if (state == null) {
                        booksListAdapter.setLoadState(LoadState.DONE);
                    } else {
                        booksListAdapter.setLoadState(state);
                    }
                }
                booksListAdapter.notifyDataSetChanged();

            }

        });

    }

    RetryCallback retryCallback = new RetryCallback() {
        @Override
        public void retry() {
            viewModel.retry();
        }
    };

    private void initAdapter() {

        booksListAdapter = new BooksListAdapter(retryCallback);
        recyclerView.setLayoutManager(new CustomLinearLayoutManager(getActivity()));
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(booksListAdapter);
        recyclerView.getRecycledViewPool().setMaxRecycledViews(0,10);
        recyclerView.setItemViewCacheSize(10);

        viewModel.getBooksList().observe(getActivity(), new Observer<PagedList<Item>>() {
            @Override
            public void onChanged(PagedList<Item> articles) {
                if(articles!=null) {
                    booksListAdapter.submitList(articles);
                }
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        headerTitle.setText(getResources().getString(R.string.article_list));
        imgShare.setVisibility(View.GONE);
        imgNavigation.setVisibility(View.GONE);

        keywordEditText.setText(Cache.getInstance().getKeyword());

    }
}
