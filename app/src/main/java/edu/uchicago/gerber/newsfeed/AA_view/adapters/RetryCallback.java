package edu.uchicago.gerber.newsfeed.AA_view.adapters;

public interface RetryCallback {
    void retry();
}