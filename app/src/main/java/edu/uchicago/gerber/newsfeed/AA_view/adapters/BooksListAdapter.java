package edu.uchicago.gerber.newsfeed.AA_view.adapters;

import android.annotation.SuppressLint;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.Random;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.navigation.Navigation;
import androidx.paging.PagedListAdapter;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

import edu.uchicago.gerber.newsfeed.CC_model.models.Item;
import edu.uchicago.gerber.newsfeed.CC_model.utils.Constants;
import edu.uchicago.gerber.newsfeed.CC_model.utils.LoadState;
import edu.uchicago.gerber.newsfeed.R;


public class BooksListAdapter extends PagedListAdapter<Item, RecyclerView.ViewHolder> {

    private int DATA_VIEW_TYPE = 1;
    private int FOOTER_VIEW_TYPE = 2;

    private RetryCallback retry;

    private LoadState loadState = LoadState.LOADING;

    public BooksListAdapter(RetryCallback retryCallback) {
        super(NewsDiffCallback);
        this.retry = retryCallback;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == DATA_VIEW_TYPE)
            return BooksViewHolder.create(parent);
        else
            return ListFooterViewHolder.create(parent);
    }

    @Override
    public int getItemViewType(int position) {
        if (position < super.getItemCount())
            return DATA_VIEW_TYPE;
        else
            return FOOTER_VIEW_TYPE;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (getItemViewType(position) == DATA_VIEW_TYPE)
            ((BooksViewHolder) holder).bind(getItem(position));
        else
            ((ListFooterViewHolder) holder).bind(loadState, retry);
    }

    @Override
    public int getItemCount() {
        if (hasFooter()) {
            return super.getItemCount() + 1;
        } else {
            return super.getItemCount();
        }
    }

    Boolean hasFooter() {
        return super.getItemCount() != 0 && (loadState == LoadState.LOADING || loadState == LoadState.ERROR);
    }

    public void setLoadState(LoadState loadState) {
        this.loadState = loadState;
        notifyItemChanged(super.getItemCount());
    }

    public static final DiffUtil.ItemCallback<Item> NewsDiffCallback =
            new DiffUtil.ItemCallback<Item>() {
                @Override
                public boolean areItemsTheSame(
                        @NonNull Item oldArticle, @NonNull Item newArticle) {
                    // User properties may have changed if reloaded from the DB, but ID is fixed
                    if(oldArticle!=null)
                        return oldArticle.getVolumeInfo().getTitle().equals(newArticle.getVolumeInfo().getTitle());
                    else
                        return true;
                }

                @SuppressLint("DiffUtilEquals")
                @Override
                public boolean areContentsTheSame(
                        @NonNull Item oldArticle, @NonNull Item newArticle) {
                    // NOTE: if you use equals, your object must properly override Object#equals()
                    // Incorrectly returning false here will result in too many animations.
                    if(oldArticle!=null && newArticle!=null)
                        return oldArticle.equals(newArticle);
                    else
                        return true;
                }
            };
}

//inner class
class BooksViewHolder extends RecyclerView.ViewHolder {
     ImageView newsBanner;
     TextView bookItemTitle;
     TextView bookItemAuthors, bookItemPublishedDate;

    //need to reference to the containerView to communicate with the operating system
    public static View containerView;


    BooksViewHolder(View itemView) {
        super(itemView);

        newsBanner =  itemView.findViewById(R.id.img_news_banner);
        bookItemTitle =  itemView.findViewById(R.id.book_item_title);
        bookItemAuthors =  itemView.findViewById(R.id.book_item_authors);
        bookItemPublishedDate =  itemView.findViewById(R.id.book_item_publishedDate);

    }


    @RequiresApi(api = Build.VERSION_CODES.O)
    public void bind(Item item) {
        if (item != null) {
            bookItemTitle.setText(item.getVolumeInfo().getTitle());
            if (null != item.getVolumeInfo() && null != item.getVolumeInfo().getImageLinks() && null != item.getVolumeInfo().getImageLinks().getSmallThumbnail()) {

                try {
                    Glide.with(containerView.getContext())
                            .load(item.getVolumeInfo().getImageLinks().getSmallThumbnail())
                            .into(newsBanner);
                } catch (Exception e) {
                    e.getMessage();
                }

            }

            if (item.getVolumeInfo().getAuthors() != null) {
                String authors = String.join(", ", item.getVolumeInfo().getAuthors());
                bookItemAuthors.setText(authors);
            }

            bookItemPublishedDate.setText(item.getVolumeInfo().getPublishedDate());

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Bundle bookVolumeData = new Bundle();
                    //refactor
                    if (null != item.getVolumeInfo().getAuthors())
                        bookVolumeData.putString(Constants.author, item.getVolumeInfo().getAuthors().toString());

                    if (null != item.getVolumeInfo().getTitle())
                        bookVolumeData.putString(Constants.title, item.getVolumeInfo().getTitle());

                    if (null != item.getVolumeInfo().getPublishedDate())
                        bookVolumeData.putString(Constants.year, item.getVolumeInfo().getPublishedDate());

                    if (null != item.getVolumeInfo().getDescription())
                        bookVolumeData.putString(Constants.desc, item.getVolumeInfo().getDescription());

                    if (null != item.getVolumeInfo().getImageLinks()  && null != item.getVolumeInfo().getImageLinks().getSmallThumbnail() )
                        bookVolumeData.putString(Constants.imageUrlLarge, item.getVolumeInfo().getImageLinks().getSmallThumbnail());

                    //pass in a walmart address to allow testing navigation
                    String[] walmarts = containerView.getContext().getResources().getStringArray(R.array.walmarts);
                    bookVolumeData.putString(Constants.address, walmarts[new Random().nextInt(walmarts.length)]);
                    Navigation.findNavController(view).navigate(R.id.action_resultsListFragment_to_detailFragment,bookVolumeData);



                }
            });
        }
    }



    public static BooksViewHolder create(ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_news, parent, false);
        containerView = parent;
        return new BooksViewHolder(view);
    }


}


//inner class
class ListFooterViewHolder extends RecyclerView.ViewHolder {

    ProgressBar progressBar;
    TextView errorText;
    RetryCallback retry;

    ListFooterViewHolder(View itemView) {
        super(itemView);
        progressBar = itemView.findViewById(R.id.progress_bar);
        errorText = itemView.findViewById(R.id.txt_error);

    }

    public void bind(LoadState status, RetryCallback retryCallback) {
        this.retry = retryCallback;
        if (status == LoadState.LOADING)
            progressBar.setVisibility(View.VISIBLE);
        else
            progressBar.setVisibility(View.INVISIBLE);
        if (status == LoadState.ERROR) {
            errorText.setVisibility(View.VISIBLE);
        } else {
            errorText.setVisibility(View.INVISIBLE);
        }

        errorText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                retry.retry();
            }
        });
    }

    public static ListFooterViewHolder create(ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_list_footer, parent, false);
        return new ListFooterViewHolder(view);
    }

}