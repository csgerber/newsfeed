package edu.uchicago.gerber.newsfeed.CC_model.repositories;

import edu.uchicago.gerber.newsfeed.CC_model.apis.BookService;
import edu.uchicago.gerber.newsfeed.CC_model.apis.BookService;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class BookRepository {
    public static BookService getService() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://www.googleapis.com/")
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        return retrofit.create(BookService.class);
    }
}
