package edu.uchicago.gerber.newsfeed.AA_view.views;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.bumptech.glide.Glide;

import edu.uchicago.gerber.newsfeed.CC_model.utils.Constants;
import edu.uchicago.gerber.newsfeed.R;


public class DetailFragment extends Fragment {

    private Button btnReturnPrevious;
    private TextView authorName;
    private TextView title;
    private TextView year;
    private TextView description;
    private ImageView imgBook;
    private String link, address;
    private TextView headerTitle;
    private Button imgShare, imgNavigation;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.frag_detail, container, false);

        authorName = view.findViewById(R.id.txt_authorName);
        title = view.findViewById(R.id.txt_titleName);
        year = view.findViewById(R.id.txt_yearName);
        description = view.findViewById(R.id.txt_descName);
        imgBook = view.findViewById(R.id.img_book);
        btnReturnPrevious = view.findViewById(R.id.btnReturnPrevious);

        //title bar elements
        headerTitle = view.findViewById(R.id.header_title);
        imgShare = view.findViewById(R.id.imgShare);
        imgNavigation = view.findViewById(R.id.imgNavigation);

        //set title bar elements
        headerTitle.setText(getResources().getString(R.string.article_detail));
        imgShare.setVisibility(View.VISIBLE);
        imgNavigation.setVisibility(View.VISIBLE);


        //get the data from the args and set to the fields of this frag
        fetchDataFromBundleArguments(getArguments());

        //add behavior
        btnReturnPrevious.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //use Android to manage the fragment backstack for you. This is preferred to calling Navigation
                getActivity().onBackPressed();
               // Navigation.findNavController(getView()).navigate(R.id.resultsListFragment);

            }
        });

        imgShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent sendIntent = new Intent(Intent.ACTION_SEND);
                sendIntent.setType("text/plain");
                sendIntent.putExtra(Intent.EXTRA_TEXT, title.getText().toString().trim() + "\n \n" + description.getText().toString().trim());
                startActivity(sendIntent);
            }
        });

        imgNavigation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
                        Uri.parse("google.navigation:q=" + address));
                getActivity().startActivity(intent);
            }
        });

        return view;
    }



    private void fetchDataFromBundleArguments(Bundle bundle) {
        if (bundle != null) {
            title.setText(getArguments().getString(Constants.title));
            year.setText(getArguments().getString(Constants.year));
            description.setText(getArguments().getString(Constants.desc));
            authorName.setText(getArguments().getString(Constants.author));
            link = getArguments().getString(Constants.imageUrlLarge);
            address = getArguments().getString(Constants.address);

            Glide.with(getActivity())
                    .load(link)
                    .into(imgBook);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        headerTitle.setText(getResources().getString(R.string.article_detail));
        imgShare.setVisibility(View.VISIBLE);
        imgNavigation.setVisibility(View.VISIBLE);
    }
}
