package edu.uchicago.gerber.newsfeed.CC_model.apis;


import edu.uchicago.gerber.newsfeed.CC_model.models.VolumesResponse;
import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface BookService {

    @GET("/books/v1/volumes")
    //DisposableSingleObserver
    Single<VolumesResponse> searchVolumes(
            @Query("q") String query,
            @Query("maxResults") int maxResults,
            @Query("startIndex") int startIndex
    );
}
