package edu.uchicago.gerber.newsfeed.BB_viewmodel.viewmodels;

import android.util.Log;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Transformations;
import androidx.lifecycle.ViewModel;
import androidx.paging.DataSource;
import androidx.paging.LivePagedListBuilder;
import androidx.paging.PageKeyedDataSource;
import androidx.paging.PagedList;


import edu.uchicago.gerber.newsfeed.CC_model.apis.BookService;
import edu.uchicago.gerber.newsfeed.CC_model.cache.Cache;
import edu.uchicago.gerber.newsfeed.CC_model.models.Item;
import edu.uchicago.gerber.newsfeed.CC_model.models.VolumesResponse;
import edu.uchicago.gerber.newsfeed.CC_model.repositories.BookRepository;
import edu.uchicago.gerber.newsfeed.CC_model.utils.LoadState;
import io.reactivex.Completable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Action;
import io.reactivex.functions.BiConsumer;
import io.reactivex.schedulers.Schedulers;

public class BooksListViewModel extends ViewModel {

    //the data
    public LiveData<PagedList<Item>> booksList;


    //helpers
    private final CompositeDisposable compositeDisposable;
    private final BooksDataSourceFactory bookDataSourceFacotory;


    public BooksListViewModel() {
        super();
        compositeDisposable = new CompositeDisposable();
        bookDataSourceFacotory = new BooksDataSourceFactory(compositeDisposable, BookRepository.getService());
        final int PAGE_SIZE = 10;
        PagedList.Config config = new PagedList.Config.Builder()
                .setPageSize(PAGE_SIZE)
                .setInitialLoadSizeHint(PAGE_SIZE)
                .setEnablePlaceholders(false)
                .build();

        booksList = new LivePagedListBuilder(bookDataSourceFacotory, config).build();

    }

    //get the state
    public LiveData<LoadState> getState() {

        return Transformations.switchMap(
                bookDataSourceFacotory.newsDataSourceLiveData,
                BooksPagedKeyedDataSource::getState
        );
    }

    //return the list
    public LiveData<PagedList<Item>> getBooksList() {
        return booksList;
    }


    public void retry() {
        bookDataSourceFacotory.newsDataSourceLiveData.getValue().retry();
    }

    public Boolean listIsEmpty() {
        if (booksList.getValue() == null) {
            return true;
        } else {
            return booksList.getValue().isEmpty();
        }
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        compositeDisposable.dispose();
    }


}

//inner class
class BooksDataSourceFactory extends DataSource.Factory<Integer, Item> {
    public MutableLiveData<BooksPagedKeyedDataSource> newsDataSourceLiveData = new MutableLiveData<BooksPagedKeyedDataSource>();
    public CompositeDisposable compositeDisposable;
    public BookService bookService;

    public BooksDataSourceFactory(CompositeDisposable compositeDisposable, BookService bookService) {
        this.compositeDisposable = compositeDisposable;
        this.bookService = bookService;
    }

    @NonNull
    @Override
    public DataSource<Integer, Item> create() {
        BooksPagedKeyedDataSource booksPagedKeyedDataSource = new BooksPagedKeyedDataSource(bookService, compositeDisposable);
        newsDataSourceLiveData.postValue(booksPagedKeyedDataSource);
        return booksPagedKeyedDataSource;
    }
}

//inner class
class BooksPagedKeyedDataSource extends PageKeyedDataSource<Integer, Item> {

    BookService bookService;
    CompositeDisposable compositeDisposable;
    MutableLiveData<LoadState> state = new MutableLiveData<>();
    Completable retryCompletable = null;


    private final int MAX_RESULT = 10;
    int startIndex = 0;

    public MutableLiveData<LoadState> getState() {
        return state;
    }

    BooksPagedKeyedDataSource(BookService bookService,
                              CompositeDisposable compositeDisposable) {
        super();
        //this registers the BooksPagedKeyedDataSource as a subscriber.
        //notice the method below annotated with @Subscribe(threadMode = ThreadMode.MAIN)
        EventBus.getDefault().register(this);
        this.bookService = bookService;
        this.compositeDisposable = compositeDisposable;
    }

    @Override
    public void loadInitial(@NonNull final LoadInitialParams<Integer> params, @NonNull final LoadInitialCallback<Integer, Item> callback) {
        updateState(LoadState.LOADING);

        //this is a set of Disposables
        compositeDisposable.add(
                bookService.searchVolumes(Cache.getInstance().getKeyword(), MAX_RESULT, startIndex)
                        .subscribe(new BiConsumer<VolumesResponse, Throwable>() {
                            @Override
                            public void accept(VolumesResponse response, Throwable throwable) throws Exception {

                                if (response != null && response.getItems() != null) {
                                    Log.d("RESPONSE_CONSUME_LIA", response.getItems().get(0).getVolumeInfo().getAuthors().get(0).toString() + "");
                                    updateState(LoadState.DONE);
                                    callback.onResult(response.getItems(), null, startIndex);


                                } else {
                                    updateState(LoadState.NODATA);
                                }

                                if (throwable != null) {
                                    updateState(LoadState.NODATA);
                                    setRetry(new Action() {
                                        @Override
                                        public void run() throws Exception {
                                            Log.d("RESPONSE_CONSUME_LIE", response + "");
                                            loadInitial(params, callback);
                                        }
                                    });
                                }

                            }
                        }));


    }

    @Override
    public void loadBefore(@NonNull LoadParams<Integer> params, @NonNull LoadCallback<Integer, Item> callback) {

    }

    @Override
    public void loadAfter(@NonNull final LoadParams<Integer> params, @NonNull final LoadCallback<Integer, Item> callback) {
        updateState(LoadState.LOADING);

        startIndex = startIndex + 10;

        Log.d("PAGING", params.requestedLoadSize + ":" + params.key);

        compositeDisposable.add(
                bookService.searchVolumes(Cache.getInstance().getKeyword(), MAX_RESULT, startIndex)
                        .subscribe(new BiConsumer<VolumesResponse, Throwable>() {
                            @Override
                            public void accept(VolumesResponse response, Throwable throwable) throws Exception {
                                if (response != null && response.getItems() != null) {
                                    Log.d("RESPONSE_CONSUME_LAA", params.key + "");
                                    updateState(LoadState.DONE);
                                    callback.onResult(response.getItems(), startIndex);
                                }else{
                                    updateState(LoadState.ERROR);
                                    setRetry(new Action() {
                                        @Override
                                        public void run() throws Exception {
                                            Log.d("RESPONSE_CONSUME_LAE", params.key + "");
                                            loadAfter(params, callback);
                                        }
                                    });
                                }

                            }
                        }));

    }

    private void updateState(LoadState loadState) {
        this.state.postValue(loadState);
        Log.d("EVENT_BUS_UPDATE_STATUS",   startIndex + " " + loadState);

    }

    public void retry() {
        if (retryCompletable != null) {
            compositeDisposable.add(retryCompletable
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe());
        }
    }

    private void setRetry(Action action) {
        if (action == null) {
            retryCompletable = null;
        } else {
            retryCompletable = Completable.fromAction(action);
        }

    }

    //the IDE does not recognize this method, but the EventBus IS actually using it.
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(String keyword) {

        invalidate();
        //make sure to call dispose, otherwise you will collect
        compositeDisposable.dispose();
        Log.d("EVENT_BUS_AFTER",    compositeDisposable.size() + " : INVALIDATE AND DISPOSE EXISTING CALLBACKS, NOW LISTENING for " + keyword + " VolumeResponses from api...");
    }

}
